<?php


namespace FastApi\SearchBox;


class SSelect extends  BaseSearch
{
    public $type = "select";
    public $choices = [];

    public function __construct($name,$expression = "eq")
    {
        $this->name = $name;
        $this->expression = $expression;
    }
    public function setChoices($choices)
    {
        $this->choices = $choices;
        return $this;
    }

    public function createData()
    {
        $baseData = $this->getBaseData();
        $baseData["type"] = $this->type;
        $baseData["choices"] = $this->choices;
        return $baseData;
    }
}
