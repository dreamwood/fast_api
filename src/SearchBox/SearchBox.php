<?php


namespace FastApi\SearchBox;


class SearchBox
{
    private $data = [];
    private $where = [];
    public $hasCondition = false;
    public $excel = false;

    public function addText ($name,$expression = "eq")
    {
        return $this->data[] =  new SText($name,$expression);
    }
    public function addDate ($name,$expression = "eq")
    {
        return $this->data[] =  new SDate($name,$expression);
    }
    public function addSelect ($name,$expression = "eq")
    {
        return $this->data[] =  new SSelect($name,$expression);
    }


    public function setExcel(bool $bool)
    {
        $this->excel = $bool;
        return $this;
    }
    public function createData()
    {
        /**@var $conf \FastApi\SearchBox\BaseSearch */
        foreach ($this->data as $conf) {
            if (!isset($this->where[$conf->expression])) {
                $this->where[$conf->expression] = [];
            }
            $this->where[$conf->expression][$conf->name]='';
            $this->hasCondition = true;
        }

        return [
            "show"=>$this->hasCondition,
            "excel"=>$this->excel,
            "box"=>$this->data,
            "attr"=>$this->where
        ];
    }

}
