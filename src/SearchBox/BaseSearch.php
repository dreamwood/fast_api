<?php


namespace FastApi\SearchBox;


class BaseSearch
{
    public $expression = "eq";
    public $name = "";
    public $label = "";
    public $helpText = "";
    public $width = "3";

    public function setExpression($expr){
        $this->expression = $expr;
        return $this;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    public function setHelpText($helpText)
    {
        $this->helpText = $helpText;
        return $this;
    }

    public function setWidth($width)
    {
        $this->width = $width;
        return $this;
    }

    protected function getBaseData()
    {
        return [
            'name' => $this->name,
            'label' => $this->label,
            'expr' => $this->expression,
            'text' => $this->helpText,
            'width' => $this->width
        ];
    }
}
