<?php


namespace FastApi\SearchBox;


class SDate extends  BaseSearch
{
    public $type = "date";

    public function __construct($name,$expression = "eq")
    {
        $this->name = $name;
        $this->expression = $expression;
    }

    public function createData()
    {
        $baseData = $this->getBaseData();
        $baseData["type"] = $this->type;
        return $baseData;
    }
}
