<?php


namespace FastApi\HTML;


class Colorful
{

    /*常用*/
    static function setGreen(&$str){$str=self::textColor($str,self::green(),self::cf());}
    static function setGreenBg(&$str){$str=self::textColor($str,self::cf(),self::green());}
    static function setOrange(&$str){$str=self::textColor($str,self::orange(),self::cf());}
    static function setOrangeBg(&$str){$str=self::textColor($str,self::cf(),self::orange());}
    static function setRed(&$str){$str=self::textColor($str,self::red(),self::cf());}
    static function setRedBg(&$str){$str=self::textColor($str,self::cf(),self::red());}
    static function setBlue(&$str){$str=self::textColor($str,self::blue(),self::cf());}
    static function setBlueBg(&$str){$str=self::textColor($str,self::cf(),self::blue());}


    static function setGreenInArray(&$data,$key){foreach ($data as &$row){$row[$key]=self::textColor($row[$key],self::green(),self::cf());}}
    static function setGreenBgInArray(&$data,$key){foreach ($data as &$row){$row[$key]=self::textColor($row[$key],self::cf(),self::green());}}
    static function setOrangeInArray(&$data,$key){foreach ($data as &$row){$row[$key]=self::textColor($row[$key],self::orange(),self::cf());}}
    static function setOrangeBgInArray(&$data,$key){foreach ($data as &$row){$row[$key]=self::textColor($row[$key],self::cf(),self::orange());}}
    static function setRedInArray(&$data,$key){foreach ($data as &$row){$row[$key]=self::textColor($row[$key],self::red(),self::cf());}}
    static function setRedBgInArray(&$data,$key){foreach ($data as &$row){$row[$key]=self::textColor($row[$key],self::cf(),self::red());}}
    static function setBlueInArray(&$data,$key){foreach ($data as &$row){$row[$key]=self::textColor($row[$key],self::blue(),self::cf());}}
    static function setBlueBgInArray(&$data,$key){foreach ($data as &$row){$row[$key]=self::textColor($row[$key],self::cf(),self::blue());}}



    /*返回色值*/
    static function cF(){return "f";}
    static function c0(){return "0";}
    static function red(){return "red";}
    static function pink(){return "Pink";}
    static function Purple(){return "Purple";}
    static function deepPurple(){return "Deep Purple";}
    static function indigo(){return "Indigo";}
    static function blue(){return "Blue";}
    static function lightBlue(){return "Light Blue";}
    static function cyan(){return "Cyan";}
    static function teal(){return "Teal";}
    static function green(){return "Green";}
    static function lightGreen(){return "Light Green";}
    static function lime(){return "Lime";}
    static function yellow(){return "Yellow";}
    static function amber(){return "Amber";}
    static function orange(){return "Orange";}
    static function deepOrange(){return "Deep Orange";}
    static function brown(){return "Brown";}
    static function blueGrey(){return "Blue Grey";}
    static function grey(){return "Grey";}

    /**
     * @param $text
     * @param $color
     * @param $backGround
     * @return string
     */
    static function textColor($text,$color,$backGround){
        $style="";
        switch ($color) {
            case "f";
                $style.="color: #fafafa;";break;
            case "0";
                $style.="color: #424242;";break;
            case "Red";
                $style.="color: #f44336;";break;
            case "Pink";
                $style.="color: #e91e63;";break;
            case "Purple";
                $style.="color: #9c27b0;";break;
            case "Deep Purple";
                $style.="color: #673ab7;";break;
            case "Indigo";
                $style.="color: #3f51b5;";break;
            case "Blue";
                $style.="color: #2196f3;";break;
            case "Light Blue";
                $style.="color: #03a9f4;";break;
            case "Cyan";
                $style.="color: #00bcd4;";break;
            case "Teal";
                $style.="color: #009688;";break;
            case "Green";
                $style.="color: #4caf50;";break;
            case "Light Green";
                $style.="color: #8bc34a;";break;
            case "Lime";
                $style.="color: #cddc39;";break;
            case "Yellow";
                $style.="color: #ffeb3b;";break;
            case "Amber";
                $style.="color: #ffc107;";break;
            case "Orange";
                $style.="color: #ff9800;";break;
            case "Deep Orange";
                $style.="color: #ff5722;";break;
            case "Brown";
                $style.="color: #795548;";break;
            case "Blue Grey";
                $style.="color: #607d8b;";break;
            case "Grey";
                $style.="color: #9e9e9e;";break;
            default:
//            $style.="color: #424242;";break;
        }
        switch ($backGround) {
            case "0";
                $style.="background-color:#424242;";break;
            case "f";
                $style.="background-color:#fafafa;";break;
            case "Red";
                $style.="background-color:#f44336;";break;
            case "Pink";
                $style.="background-color:#e91e63;";break;
            case "Purple";
                $style.="background-color:#9c27b0;";break;
            case "Deep Purple";
                $style.="background-color:#673ab7;";break;
            case "Indigo";
                $style.="background-color:#3f51b5;";break;
            case "Blue";
                $style.="background-color:#2196f3;";break;
            case "Light Blue";
                $style.="background-color:#03a9f4;";break;
            case "Cyan";
                $style.="background-color:#00bcd4;";break;
            case "Teal";
                $style.="background-color:#009688;";break;
            case "Green";
                $style.="background-color:#4caf50;";break;
            case "Light Green";
                $style.="background-color:#8bc34a;";break;
            case "Lime";
                $style.="background-color:#cddc39;";break;
            case "Yellow";
                $style.="background-color:#ffeb3b;";break;
            case "Amber";
                $style.="background-color:#ffc107;";break;
            case "Orange";
                $style.="background-color:#ff9800;";break;
            case "Deep Orange";
                $style.="background-color:#ff5722;";break;
            case "Brown";
                $style.="background-color:#795548;";break;
            case "Blue Grey";
                $style.="background-color:#607d8b;";break;
            case "Grey";
                $style.="background-color:#9e9e9e;";break;
            default:
//            $style.="background-color:#fafafa;";break;
        }
        return "<span style='border-radius: 3px; padding: 3px 7px;{$style}'>{$text}</span>";
    }
}
