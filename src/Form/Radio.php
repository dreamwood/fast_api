<?php
/**nij
 * Created by PhpStorm.
 * User: Machenike
 * Date: 2019/7/16
 * Time: 9:04
 */

namespace FastApi\Form;




class Radio extends BaseType
{

    public $type = "radio";
    public $name = "";
    public $label = "";
    public $class = "";
    public $entity = "en";
    public $width = 12;
    public $rules = [];
    public $placeholder = "";
    public $enable = true;



    public $choices = [];


    function __construct($name,$label, $choices)
    {
        $this->name = $name;
        $this->label = $label;
        $this->choices = $choices;
        $this->placeholder = "请选择".$label;
        return $this;
    }

    public function getData()
    {
        $data["attr"] = [
            "entity"        =>$this->entity
            ,"width"         =>$this->width
            ,"name"         =>$this->name
            ,"label"        =>$this->label
            ,"class"        =>$this->class
            ,"rules"        =>$this->rules
            ,"placeholder"  =>$this->placeholder
            ,"type"         =>$this->type
            ,"choices"         =>$this->choices
            ,"enable"       =>$this->enable
        ];
        $data["label"] = $this->label;
//        $data["choices"] = $this->choices;
        return $data;
    }
}
