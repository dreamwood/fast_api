<?php
/**nij
 * Created by PhpStorm.
 * User: Machenike
 * Date: 2019/7/16
 * Time: 9:04
 */

namespace FastApi\Form;




class Line extends BaseType
{

    public $type = "line";
    public $name = "";
    public $label = "";
    public $class = "";
    public $entity = "en";
    public $width = 12;
    public $rules = [];
    public $placeholder = "";

    public $enable = true;


    function __construct($text,$name='line',$label='▼')
    {
        $this->name = $name;
        $this->label = $label;
        $this->placeholder = $text;
//        return $this;
    }

    public function getData()
    {
        $data["attr"] = [
            "entity"        =>$this->entity
            ,"width"         =>$this->width
            ,"name"         =>$this->name.uniqid()
            ,"label"        =>$this->label
            ,"class"        =>$this->class
            ,"rules"        =>$this->rules
            ,"placeholder"  =>$this->placeholder
            ,"type"         =>$this->type
            ,"enable"       =>$this->enable
        ];
        $data["label"] = $this->label;
        return $data;
    }

    public function disable()
    {
        $this->enable = false;
        return $this;
    }
}
