<?php
/**nij
 * Created by PhpStorm.
 * User: Machenike
 * Date: 2019/7/16
 * Time: 9:04
 */

namespace FastApi\Form;




class Date extends BaseType
{

    public $type = "date";
    public $name = "";
    public $label = "";
    public $class = "";
    public $entity = "en";
    public $width = 12;
    public $rules = [];
    public $isMonth = "0";
    public $placeholder = "";
    public $format = "YYYY-MM-DD";


    public $enable = true;


    function __construct($name,$label)
    {
        $this->name = $name;
        $this->label = $label;
        $this->placeholder = "请输入".$label;
        return $this;
    }

    public function getData()
    {
        $data["attr"] = [
            "entity"        =>$this->entity
            ,"width"         =>$this->width
            ,"name"         =>$this->name
            ,"label"        =>$this->label
            ,"class"        =>$this->class
            ,"rules"        =>$this->rules
            ,"placeholder"  =>$this->placeholder
            ,"type"         =>$this->type
            ,"format"       =>$this->format
            ,"isMonth"      =>$this->isMonth
            ,"enable"       =>$this->enable
        ];
        $data["label"] = $this->label;
        return $data;
    }

    public function setFormat($format = "")
    {
        $this->format = $format;
        return $this;
    }

    public function setFormatYYYYMM()
    {
        $this->setFormat("YYYYMM");
        return $this;
    }

    public function isMonth()
    {
        $this->isMonth = 1;
        return $this;
    }
}
