<?php
/**nij
 * Created by PhpStorm.
 * User: Machenike
 * Date: 2019/7/16
 * Time: 9:04
 */

namespace FastApi\Form;



class Arrays extends BaseType
{

    public $type = "array";
    public $name = "";
    public $label = "";
    public $labels = "";
    public $tips = "";
    public $class = "";
    public $entity = "en";
    public $mod = true;
    public $width = 12;
    public $rules = [];
    public $placeholder = "";
    public $find = "0";//是否允许搜索
    public $enable = true;


    public $choices = [];
    public $onChange = "";
    public $enName = "";//自动检索路由参数
    public $multi = true;//自动检索路由参数

    public $itemWidth =[];

    function __construct($name,$label, $choices=[])
    {
        $this->name = $name;
        $this->label = $label;
        $this->choices = $choices;
        $this->placeholder = "请选择".$label;
        return $this;
    }

    public function getData()
    {
        $data["attr"] = [
            "entity"        =>$this->entity
            ,"width"         =>$this->width
            ,"name"         =>$this->name
            ,"label"        =>$this->label
            ,"labels"        =>$this->labels
            ,"tips"         =>$this->tips
            ,"class"        =>$this->class
            ,"rules"        =>$this->rules
            ,"placeholder"  =>$this->placeholder
            ,"type"         =>$this->type
            ,"mod"         =>$this->mod
            ,"find"         =>$this->find
            ,"enable"       =>$this->enable
            ,"onChange"     =>$this->onChange
            ,"choices"     =>$this->choices
            ,"enName"     =>$this->enName
            ,"multi"     =>$this->multi
            ,"itemWidth"     =>$this->itemWidth
        ];
        $data["label"] = $this->label;
        return $data;
    }

    public function enableFind()
    {
        $this->find = 1;
        return $this;
    }

    public function setMod($bool = false)
    {
        $this->mod = $bool;
        return $this;
    }

    public function disable()
    {
        $this->enable = false;
        return $this;
    }

    public function setLabel($labels)
    {
        $this->labels = $labels;
        return $this;
    }

    public function setTips($tips)
    {
        $this->tips = $tips;
        return $this;
    }

    public function setPlaceholder(array $placeholder)
    {
        $this->placeholder = $placeholder;
        return $this;
    }

    public function setItemWith(array $width)
    {
        $this->itemWidth = $width;
        return $this;
    }
}
