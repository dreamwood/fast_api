<?php
/**nij
 * Created by PhpStorm.
 * User: Machenike
 * Date: 2019/7/16
 * Time: 9:04
 */

namespace FastApi\Form;


class BaseType
{
    public function ruleNotNull()
    {
//        $this->rules = substr($this->rules,0,-1)."rules.notNull,"."]";
        $this->rules[] = "rules.notNull";
        return $this;
    }
    public function ruleMin($num = 0)
    {
//        $this->rules = substr($this->rules,0,-1)."rules.min($num),"."]";
        $this->rules[] = "rules.min($num)";
        return $this;
    }
    public function ruleMax($num = 0)
    {
//        $this->rules = substr($this->rules,0,-1)."rules.max($num),"."]";
        $this->rules[] = "rules.max($num)";
        return $this;
    }
    public function ruleLen($num = 0)
    {
//        $this->rules = substr($this->rules,0,-1)."rules.len($num),"."]";
        $this->rules[] = "rules.len($num)";
        return $this;
    }
    public function ruleIsPhone()
    {
//        $this->rules = substr($this->rules,0,-1)."rules.isPhone(),"."]";
        $this->rules[] = "rules.isPhone()";
        return $this;
    }
    public function ruleisNum()
    {
//        $this->rules = substr($this->rules,0,-1)."rules.isNum(),"."]";
        $this->rules[] = "rules.isNum()";
        return $this;
    }

    public function setWidth($num = 12)
    {
        $this->width = $num;
        return $this;
    }

    public function getType()
    {
        return $this->realType;
    }

    public function getName()
    {
        return $this->name;
    }


    public function disable()
    {
        $this->enable = false;
        return $this;
    }
}
