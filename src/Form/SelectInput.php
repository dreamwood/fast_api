<?php
/**nij
 * Created by PhpStorm.
 * User: Machenike
 * Date: 2019/7/16
 * Time: 9:04
 */

namespace FastApi\Form;



class SelectInput extends BaseType
{

    public $type = "selectinput";
    public $name = "";
    public $label = "";
    public $class = "";
    public $entity = "en";
    public $width = 12;
    public $rules = [];
    public $placeholder = "";
    public $find = "0";//是否允许搜索
    public $enable = true;
    public $isText = false;
    public $isTag = false;


    public $choices = [];
    public $onChange = "";


    function __construct($name,$label, $choices)
    {
        $this->name = $name;
        $this->label = $label;
        $this->choices = $choices;
        $this->placeholder = "请选择".$label;
        return $this;
    }

    public function getData()
    {
        $data["attr"] = [
            "entity"        =>$this->entity
            ,"width"         =>$this->width
            ,"name"         =>$this->name
            ,"label"        =>$this->label
            ,"class"        =>$this->class
            ,"rules"        =>$this->rules
            ,"placeholder"  =>$this->placeholder
            ,"type"         =>$this->type
            ,"find"         =>$this->find
            ,"enable"       =>$this->enable
            ,"onChange"     =>$this->onChange
            ,"choices"     =>json_encode(array_values($this->choices),JSON_UNESCAPED_UNICODE)
            ,"isText"     =>$this->isText
            ,"isTag"     =>$this->isTag
        ];
        $data["label"] = $this->label;
//        $data["choices"] = $this->choices;
        return $data;
    }

    public function setText()
    {
        $this->isText = true;
        return $this;
    }
    public function setTag()
    {
        $this->isTag = true;
        return $this;
    }

    public function enableFind()
    {
        $this->find = 1;
        return $this;
    }

    public function onChange($funcName)
    {
        $this->onChange = $funcName;
        return $this;
    }


    public function disable()
    {
        $this->enable = false;
        return $this;
    }
}
