<?php


namespace FastApi\ActionBox;


class ActionDate extends  BaseActionBox
{
    public $type = "date";

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function createData()
    {
        $baseData = $this->getBaseData();
        $baseData["type"] = $this->type;
        return $baseData;
    }
}
