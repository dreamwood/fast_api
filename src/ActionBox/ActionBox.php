<?php


namespace FastApi\ActionBox;


class ActionBox
{
    private $data = [];
    private $items = [];
    public $hasItem = false;

    public function addText ($name)
    {
        return $this->data[] =  new ActionText($name);
    }
    public function addDate ($name)
    {
        return $this->data[] =  new ActionDate($name);
    }
    public function addSelect ($name)
    {
        return $this->data[] =  new ActionSelect($name);
    }

    public function createData()
    {
        /**@var $conf \FastApi\ActionBox\BaseActionBox */
        foreach ($this->data as $conf) {
            $this->items[$conf->name]='';
            $this->hasItem = true;
        }
        return [
            "show"=>$this->hasItem,
            "box"=>$this->data,
            "items"=>$this->items
        ];
    }

}
