<?php


namespace FastApi\ActionBox;


class ActionText extends  BaseActionBox
{
    public $type = "text";
    public function __construct($name)
    {
        $this->name = $name;
    }

    public function createData()
    {
        $baseData = $this->getBaseData();
        $baseData["type"] = $this->type;
        return $baseData;
    }
}
