<?php


namespace FastApi\ActionBox;


class ActionSelect extends  BaseActionBox
{
    public $type = "select";
    public $choices = [];

    public function __construct($name)
    {
        $this->name = $name;
    }
    public function setChoices($choices)
    {
        $this->choices = $choices;
        return $this;
    }

    public function createData()
    {
        $baseData = $this->getBaseData();
        $baseData["type"] = $this->type;
        $baseData["choices"] = $this->choices;
        return $baseData;
    }
}
