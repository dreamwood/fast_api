<?php


namespace FastApi\HelpClasses;


class ListMeta
{
    public $list = [];
    public $page = 1;
    public $limit = 10;
    public $total = 0;

    /**
     * @return array
     */
    public function getList(): array
    {
        return $this->list;
    }

    /**
     * @param array $list
     * @return $this
     */
    public function setList(array $list): self
    {
        $this->list = $list;
        return $this;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     * @return $this
     */
    public function setPage(int $page): self
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     * @return $this
     */
    public function setLimit(int $limit): self
    {
        $this->limit = $limit;
        return $this;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     * @return $this
     */
    public function setTotal(int $total): self
    {
        $this->total = $total;
        return $this;
    }

    /**
     * @return array
     */
    public function getMeta()
    {
        return [
            'page'=>$this->getPage(),
            'limit'=>$this->getLimit(),
            'total'=>$this->getTotal(),
        ];
    }
}
